<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $guarded = [];

    public function getAll()
    {
        return Customer::latest()->paginate(10);
    }

    public function search($data)
    {
        return Customer::where('id', 'like', '%' . $data . '%')
            ->orWhere('name', 'like', '%' . $data . '%')
            ->orWhere('email', 'like', '%' . $data . '%')
            ->orWhere('phone', 'like', '%' . $data . '%')->paginate(10);
    }

    public function orders()
    {
       return $this->hasMany(Order::Class);
    }
}
