<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];

    public function getAll()
    {
        return Category::latest('id')->paginate(5);
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = \Str::slug($value);
    }

    public function searchCategory($data)
    {
        return Category::where('id', 'like', '%' . $data . '%')
            ->orWhere('name', 'like', '%' . $data . '%')
            ->orWhere('title', 'like', '%' . $data . '%')->paginate(5);
    }

    public function products()
    {
        return $this->hasMany(Product::Class);
    }
}
