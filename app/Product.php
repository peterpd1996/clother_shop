<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = [];

    public function getAll()
    {
        return Product::with('category')->latest()->paginate(10);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = \Str::slug($value);
    }

    public function quickSearch($query)
    {
        return Product::where('id', 'like', '%' . $query . '%')
            ->orWhere('name', 'like', '%' . $query . '%')
            ->orWhere('title', 'like', '%' . $query . '%')
            ->latest()->paginate(10);
    }

    public function search($data)
    {
        return Product::whereBetween('price', [$data['fromPrice'], $data['toPrice']])
            ->where('category_id', '=', $data['category'])
            ->latest()->paginate(10);
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class);
    }

}
