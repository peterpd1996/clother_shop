<?php

namespace App\Http\Controllers;

use App\Http\Requests\CustomerRequest;
use Illuminate\Http\Request;
use App\Customer;

class CustomerController extends Controller
{
    private $customer;

    public function __construct(Customer $customer)
    {
        $this->customer = $customer;
    }

    public function index()
    {
        $customers = $this->customer->getAll();
        return view('admins.customers.index', compact('customers'));
    }

    public function create()
    {
        return view('admins.customers.add');
    }

    public function store(CustomerRequest $request)
    {
        $customer = $this->customer::create($request->all());
        return redirect()->route('admin.customers.index')->with('message', trans('usually.success_add'));
    }

    public function edit($id)
    {
        $customer = $this->customer::findOrFail($id);
        return view('admins.customers.edit', compact('customer'));
    }

    public function update(CustomerRequest $request, $id)
    {
        $customer = $this->customer::findOrFail($id);
        $customer->update($request->all());
        return redirect()->route('admin.customers.index')->with('message', trans('usually.success_edit'));
    }

    public function destroy($id)
    {
        $this->customer::destroy($id);
        return back()->with('message', trans('usually.success_delete'));
    }

    public function multiDelete($customers_id)
    {
        $customers_id = json_decode($customers_id);
        $this->customer::whereIn('id', $customers_id)->delete();
        $customers = $this->customer->getAll();
        return view('admins.customers.table_customer', compact('customers'));
    }

    public function search(Request $request)
    {
        $dataCustomer = $request->get('data');
        $customers = $this->customer->search($dataCustomer);
        return view('admins.customers.table_customer', compact('customers'));
    }

    public function getcustomer(Request $request)
    {
        $phone = $request->get('phone');
        $customer = $this->customer::Where('phone', $phone)->get();
        if ($customer->count() > 0) {
            return response()->json([
                'status' => 200,
                'customer' => $customer
            ]);
        } else {
            return response()->json([
                'status' => 404,
            ]);
        }

    }

    public function storeApi(CustomerRequest $request)
    {
        $requestData = $request->all();
        $customer = $this->customer::create($requestData);
        return response()->json([
            'id' => $customer->id,
            'success_title' => trans('usually.success_title'),
            'success_body' => trans('usually.success_add')
        ], 200);
    }
}
