<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Customer;
use App\Order;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use PDF;

class OrderController extends Controller
{
    private $order;
    private $checkEnoughQuantity = true;

    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    public function index()
    {
        $orders = $this->order->getAll();
        return view('admins.orders.index', compact('orders'));
    }

    public function destroy($id)
    {
        $this->order::destroy($id);
        return redirect()->back()->with('message', trans('usually.success_delete'));
    }

    public function create()
    {
        return view('admins.orders.add');
    }

    public function orderDetail($id)
    {
        $order = $this->order::findOrFail($id);
        return view('admins.orders.order_detail', compact('order'));
    }

    public function getProduct(Request $request)
    {
        $nameOrId = $request->get('data');
        $products = $this->order->getProductToBuy($nameOrId);
        return view('admins.orders.search_buy_product', compact('products'));
    }

    public function addItem(Request $request)
    {
        $product = $request->input('data');
        $product['price'] = $this->getPrice($product['price'], $product['sale']);
        $cart = $this->getOldCart();
        $this->checkEnoughQuantity = $cart->add($product, $product['id']);
        if ($this->checkEnoughQuantity == false) {
            return 0;
        }
        $request->session()->put('cart', $cart);
        return "ok";
    }

    public function removeItem(Request $request)
    {
        $id = $request->input('data');
        $cart = $this->getOldCart();
        $cart->remove($id);
        $request->session()->put('cart', $cart);
    }

    public function updateItem(Request $request)
    {
        $data = $request->input('data');
        $cart = $this->getOldCart();
        $cart->update($data['id'], $data['qty']);
        $request->session()->put('cart', $cart);
    }

    public function getAllItems()
    {
        if (!Session::has('cart') && $this->checkEnoughQuantity === false) {
            return false;
        }
        $oldCart = Session::get('cart');
        $products = $oldCart->items;
        $totalPrice = $oldCart->totalPrice;
        return view('admins.orders.order_table', compact('products', 'totalPrice'));

    }

    public function checkOut($customer_id)
    {
        
        if (!Session::has('cart')) {
            return redirect('admin/');
        }
        $customer = Customer::findOrFail($customer_id);
        $oldCart = Session::get('cart');
        $order = $this->order->create([
            'user_id' => Auth::user()->id,
            'customer_id' => $customer_id,
            'total_price' => $oldCart->totalPrice,
        ]);
        $products = $oldCart->items;
        $totalPrice = $oldCart->totalPrice;
        $this->order->insertOrderDetail($products, $order);
        $this->order->updateQuantityProduct($products);
        return view('admins.orders.checkout', compact('customer', 'products', 'totalPrice'));
    }

    public function printPdf(Request $request, $customer_id)
    {
        if (!Session::has('cart')) {
            return redirect('admin/');
        }
        $customer = Customer::findOrFail($customer_id);
        $oldCart = Session::get('cart');
        $products = $oldCart->items;
        $totalPrice = $oldCart->totalPrice;
        $request->session()->forget('cart');
        $pdf = PDF::loadView('admins.orders.order_pdf', compact('customer', 'products', 'totalPrice'));
        return $pdf->download('order.pdf');
    }

    public function getPrice($price, $sale)
    {
        if ($sale == null) {
            return $price;
        } else {
            return $price * (100 - $sale) / 100;
        }
    }

    public function getOldCart()
    {
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        return new Cart($oldCart);
    }
}
