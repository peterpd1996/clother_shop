<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function getLogin()
    {
        return view('admins.auth.login');
    }

    public function login(UserRequest $request)
    {
       $email = $request->input('email');
       $password = $request->input('password');
       if(Auth::attempt(['email'=> $email,'password'=>$password]))
       {
           return redirect('/admin/');
       }
       else
       {
          return redirect()->back()->with('message','Tai khoan hoac mat khau khong dung');
       }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }
}
