<?php

namespace App\Http\Controllers;

use App\Category;
use App\Customer;
use App\Order;
use App\Product;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    public function index()
    {
        $productsHot = Order::topProductsHot();
        $totalCustomers = Customer::count();
        $totalProducts = Product::count();
        $totalOrders = Order::count();
        $totalCategories = Category::count();
        $totalDay = Order::whereDay('created_at', date('d'))->sum('total_price');
        $totalMonth = Order::whereMonth('created_at', date('m'))->sum('total_price');
        return view('admins.index', compact('totalMonth', 'totalDay', 'totalProducts', 'totalCustomers', 'totalOrders', 'totalCategories', 'productsHot'));
    }
}
