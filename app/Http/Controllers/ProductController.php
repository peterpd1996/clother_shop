<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Product;
use App\Traits\Image;
use Illuminate\Http\Request;
use App\Category;


class ProductController extends Controller
{
    use Image;
    private $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function index()
    {
        $products = $this->product->getAll();
        $categories = Category::all();
        return view('admins.products.index', compact('products', 'categories'));
    }

    public function create()
    {
        $categories = Category::all();
        return view('admins.products.add', compact('categories'));
    }

    public function edit($id)
    {
        $categories = Category::all();
        $product = $this->product::findOrFail($id);
        return view('admins.products.edit', compact('product', 'categories'));
    }

    public function update(ProductUpdateRequest $request, $id)
    {
        $product = $this->product::findOrFail($id);
        $dataProduct = $this->getDataProduct($request);
        if ($request->hasFile('image')) {
            $this->deleteImage($product->image);
            $imgName = $this->uploadImage($request->file('image'));
            $dataProduct['image'] = $imgName;
        }
        $product->update($dataProduct);
        return redirect()->route('admin.products.index')->with('message', trans('usually.success_edit'));
    }

    public function store(ProductRequest $request)
    {
        $dataProduct = $this->getDataProduct($request);
        $imgName = $this->uploadImage($request->file('image'));
        $dataProduct['image'] = $imgName;
        $this->product->create($dataProduct);
        return redirect()->route('admin.products.index')->with('message', trans('usually.success_add'));
    }

    public function getDataProduct($request)
    {
        $dataProduct = $request->all();
        $dataProduct['price'] = (int)str_replace(",", "", $dataProduct['price']);
        $dataProduct['slug'] = $request->input('name');
        return $dataProduct;
    }

    public function destroy($id)
    {
        $product = $this->product::findOrFail($id);
        $this->deleteImage($product->image);
        $product->delete();
        return back()->with('message', trans('usually.success_delete'));
    }

    public function multiDelete($products_id)
    {
        $products_id = json_decode($products_id);
        $imagesProduct = $this->product::find($products_id)->pluck('image');
        $this->deleteMultiImage($imagesProduct);
        $this->product::whereIn('id', $products_id)->delete();
        $products = $this->product->getAll();
        return view('admins.products.table_product', compact('products'));
    }

    public function quickSearch(Request $request)
    {
        $dataProduct = $request->get('data');
        $products = $this->product->quickSearch($dataProduct);
        return view('admins.products.table_product', compact('products'));
    }

    public function search(Request $request)
    {
        $dataProduct = $request->all();
        $dataProduct['fromPrice'] = (int)str_replace(",", "", $dataProduct['fromPrice']);
        $dataProduct['toPrice'] = (int)str_replace(",", "", $dataProduct['toPrice']);
        $products = $this->product->search($dataProduct);
        return view('admins.products.table_product', compact('products'));
    }

}
