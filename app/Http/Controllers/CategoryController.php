<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\RequestCategory;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    private $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function index()
    {
        $categories = $this->category->getAll();
        return view('admins.category.index', compact('categories'));
    }

    public function store(RequestCategory $request)
    {
        $requestData = $request->only('name', 'title');
        $requestData['slug'] = $request['name'];
        $category = $this->category->create($requestData);
        return response()->json([
            'category' => $category,
            'success_title' => trans('usually.success_title'),
            'success_body' => trans('usually.success_add')
        ], 200);
    }

    public function update(RequestCategory $request, $id)
    {
        $category = Category::find($id);
        $requestData = $request->all();
        $requestData['slug'] = $request['name'];
        $category->update($requestData);
        return response()->json([
            'category' => $category,
            'success_title' => trans('usually.success_title'),
            'success_edit' => trans('usually.success_edit'),
        ], 200);
    }

    public function destroy($id)
    {
        $this->category::destroy($id);
        return response()->json([
            'id' => $id,
            'module' => 'cate',
        ], 200);
    }

    public function search(Request $request)
    {
        $dataCategory = $request->get('data');
        $categories = $this->category->searchCategory($dataCategory);
        return view('admins.category.table_category', compact('categories'));
    }

    public function multiDelete($categories_id)
    {
        $categories_id = json_decode($categories_id);
        $this->category::whereIn('id', $categories_id)->delete();
        $categories = $this->category->getAll();
        return view('admins.category.table_category', compact('categories'));
    }
}
