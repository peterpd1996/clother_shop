<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use  App\Product;
use Illuminate\Support\Facades\DB;

class Order extends Model
{
    protected $guarded = [];

    public function getAll()
    {
        return Order::latest()->paginate(10);
    }

    public function getProductToBuy($data)
    {
        return Product::where('id', 'like', '%' . $data . '%')
            ->orWhere('name', 'like', '%' . $data . '%')->get();
    }

    public function products()
    {
        return $this->belongsToMany(Product::class)->withPivot('quantity', 'price', 'sale');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    public function insertOrderDetail($products, $order)
    {
        foreach ($products as $product) {
            $order->products()->attach(
                $product['item']['id'],
                [
                    'quantity' => $product['qty'],
                    'price' => $product['item']['price'], // price da sale roi
                    'sale' => $product['item']['sale'],
                ]);
        }
    }

    public function updateQuantityProduct($products)
    {
        foreach ($products as $product) {
            Product::find($product['item']['id'])->decrement('quantity', $product['qty']);
        }
    }

    public static function topProductsHot()
    {
        return $productHot = DB::table('order_product')
            ->join('products', 'order_product.product_id', '=', 'products.id')
            ->select('order_product.*', 'products.image', 'products.name', 'products.price', DB::raw('SUM(order_product.quantity) as total'))
            ->groupBy('order_product.product_id')
            ->orderBy('total', 'desc')
            ->take(10)
            ->get();
    }


}
