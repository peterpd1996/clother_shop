<?php

namespace App;

class Cart
{
    public $items = null;
    public $totalPrice = 0;

    public function __construct($oldCart)
    {
        if ($oldCart) {
            $this->items = $oldCart->items;
            $this->totalPrice = $oldCart->totalPrice;
        }
    }

    public function add($item, $id)
    {
        $storeItem = ['qty' => 0, 'price' => $item['price'], 'item' => $item];
        if ($this->items) {
            if (array_key_exists($id, $this->items)) {
                if ((int)$item['quantity'] <= $this->items[$id]['qty']) {
                    return false;
                }
                $storeItem = $this->items[$id];
            }
        }
        $storeItem['qty']++;
        $storeItem['price'] = $item['price'] * $storeItem['qty'];
        // luc them mk chi mac dinh them 1 item thoi
        $this->items[$id] = $storeItem;
        $this->totalPrice += $item['price'];
        return true;
    }

    public function remove($id)
    {
        if (array_key_exists($id, $this->items)) {
            $this->totalPrice -= $this->items[$id]['price'];
            unset($this->items[$id]);
        }
    }

    public function update($id, $qty)
    {
        if (array_key_exists($id, $this->items)) {
//            minh khong biet la so luong no tang hay giam nen phai tru truoc di da;
            $this->totalPrice -= $this->items[$id]['price'];
            $this->items[$id]['qty'] = $qty;
            $this->items[$id]['price'] = $this->items[$id]['item']['price'] * $qty;
            $this->totalPrice += $this->items[$id]['price'];
        }
    }

}
