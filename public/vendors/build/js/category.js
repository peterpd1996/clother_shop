$(function () {
    var id;
    let body = $('body');
    let btnAddNewCate = $('#btn-add-new');
    let btnSaveNewCate = $('#saveNewCate');
    let btnSaveEdit = $('#btnSaveEdit');
    let btnDeleteMulti = $('#delete-multi');
    let inputSearch = $('#inputSearch');

    function loadTableOrder() {
        let order = $('tbody .order');
        order.each(function (number) {
            $(this).text(number + 1); // This is your rel value
        });
    }

    function showError(errors) {
        if (errors.name) {
            $('.nameError').text(errors.name[0]);
            $('#nameError').focus();
        }
        if (errors.title) {
            $('.titleError').text(errors.title[0]);
            $('#titleError').focus();
        }
    }

    function resetError() {
        $('.nameError').text("");
        $('.titleError').text("");
    }

    function updateCategory(category) {
        let updateCate = `
                                 <td>
                                     <input type="checkbox" value="${category.id}" class="check-box">
                                </td>
                                <td class="order"></td>
                                <td>${category.name}</td>
                                <td>${category.slug}</td>
                                <td>${category.title}</td>
                                <td>
                                    <button class="btn btn-info btn-xs btnEditCate" data-target="#editCateModal"
                                            data-toggle="modal" data-id="${category.id}"
                                            data-name="${category.name}" data-title="${category.title}"><i
                                            class="fa fa-pencil"></i> {{ trans('usually.edit') }} </button>
                                    <button href="#" class="btn btn-danger btn-xs btnDeleteCate"
                                            data-id="${category.id}"><i
                                            class=" fa fa-trash-o"></i> {{trans('usually.delete')}} </button>
                                </td>
                            `;
        $('.cate_' + category.id).html(updateCate);
    }

    function loadNewCategory(category) {
        let newCate = `<tr class="even pointer cate_${category.id}">
                            <td>
                                     <input type="checkbox" value="${category.id}" class="check-box">
                            </td>
                            <td class="order"></td>
                            <td>${category.name}</td>
                            <td>${category.slug}</td>
                            <td class="titleCate">${category.title}</td>
                            <td>
                                <button class="btn btn-info btn-xs btnEditCate" data-target="#editCateModal"
                                        data-toggle="modal" data-id="${category.id}"
                                        data-name="${category.name}" data-title="${category.title}"><i
                                        class="fa fa-pencil"></i> {{ trans('usually.edit') }} </button>
                                <button href="#" class="btn btn-danger btn-xs btnDeleteCate"
                                        data-id="${category.id}"><i
                                        class=" fa fa-trash-o"></i> {{trans('usually.delete')}} </button>
                            </td>
                    </tr>`;
        $('tbody').prepend(newCate);
    }

    function searchCategory(page, query) {
        let baseUrl = "/admin/categories/search-pagination";
        searchAjax(page, baseUrl, query)
            .done(response => {
                $('#table-category').html(response);
            })
    }

    $(function () {
        btnAddNewCate.click(function () {
            resetError();
        });
        btnSaveNewCate.click(function (event) {
            event.preventDefault();
            let dataResource = $('#newResourceCategoryForm').serialize();
            let urlResource = '/admin/categories/store';
            let type = 'post';
            callApi(dataResource, urlResource, type)
                .done(response => {
                    $('#addCateModal').modal('hide');
                    $('#newResourceCategoryForm')[0].reset();
                    swal(response.success_title, response.success_body, "success");
                    $('.swal-button--confirm').click(function () {
                        loadNewCategory(response.category);
                        loadTableOrder();
                        if ($('#tbody tr').length == 6)
                            $('tbody tr:last-child').remove();
                    })
                })
                .fail(error => {
                    let errors = error.responseJSON.errors;
                    showError(errors);
                })
        });
        body.on('click', '.btnEditCate', function () {
            resetError();
            id = $(this).attr('data-id');
            let cateName = $(this).attr('data-name');
            let cateTitle = $(this).attr('data-title');
            $('.cateName').val(cateName);
            $('.cateTitle').val(cateTitle);
        });

        btnSaveEdit.click(function () {
            let dataResource = $('#editResourceCategoryForm').serialize();
            let url = '/admin/categories/' + id;
            let type = 'patch';
            callApi(dataResource, url, type)
                .done(response => {
                    $('#editCateModal').modal('hide');
                    $('#editResourceCategoryForm')[0].reset();
                    swal(response.success_title, response.success_edit, "success");
                    updateCategory(response.category);
                    loadTableOrder();
                })
                .fail(error => {
                    let errors = error.responseJSON.errors;
                    showError(errors);
                });
        });

        body.on('click', '.btnDeleteCate', function () {
            let idCate = $(this).attr('data-id');
            let url = '/admin/categories/' + idCate;
            let type = 'delete';
            destroyResource(url, type);
            loadTableOrder();
        });

        inputSearch.keyup(function () {
            let query = $(this).val();
            searchCategory(1, query);
        });

        body.on('click', '.pagination li a', function (event) {
            event.preventDefault();
            let page = $(this).attr('href').split("=")[1];
            let query = $('#inputSearch').val();
            searchCategory(page, query);
        });

        body.on('click', '.check-all', function () {
            let statusCheck = $(this).prop('checked');
            $('.check-box').prop('checked', statusCheck);
        });
        btnDeleteMulti.click(function () {
            let talbeCaterogy = $('#table-category');
            let id = [];
            $('.check-box:checked').each(function () {
                id.push($(this).val());
            });
            let categories_id = JSON.stringify(id);
            let url = "/admin/categories/multi-delete/" + categories_id;
            deleteMulti(id, url, talbeCaterogy);
        })
    })
});
