$(function () {
    let searchToBuy = $('#search-to-buy');
    let productGetBySearch = $('#product-get-by-search');
    let body = $('body');
    let idCustomer = 0;
    getAllItem();
    //customer
    let name = $('#name');
    let phone = $('#phone');
    let address = $('#address');
    let email = $('#email');
    let btnSaveNewCustomer = $('#save-new-customer');
    let checkout = $('#check-out');
    function getProductBySearch(data) {
        let url = searchToBuy.attr('data-url');
        $.ajax({
            url: url,
            type: 'GET',
            data: {data: data}
        }).done(response => {
            productGetBySearch.html(response);
        })
    }
    function getAllItem() {
        let url = searchToBuy.attr('data-urlGetAll');
        return $.ajax({
            url: url,
            type: 'GET',
        }).done(response => {
            $('#tbody').html(response);
            let totalMoney = $('#totalPrice').val();
            $('#price').text(totalMoney);
        });
    }
    function addToOrder(data) {
        let url = searchToBuy.attr('data-urlAdd');
        return $.ajax({
            url: url,
            type: 'POST',
            data: {data: data},
        })
    }
    function removeItemOrUpdate(data, url) {
        return $.ajax({
            url: url,
            type: 'POST',
            data: {data: data}
        });
    }
    function getData(product) {
        let id = product.attr('data-id');
        let name = product.attr('data-name');
        let price = product.attr('data-price');
        let image = product.attr('data-img');
        let quantity = product.attr('data-quantity');
        let sale = product.attr('data-sale');
        let dataProduct = {id: id, name: name, price: price, image: image, quantity: quantity, sale: sale};
        return dataProduct;
    }
    searchToBuy.keyup(function () {
        let data = $(this).val();
        if (data !== '') {
            getProductBySearch(data);
        } else {
            productGetBySearch.html("");
        }
    });
    body.on('click', '.product', function () {
        productGetBySearch.html("");
        searchToBuy.val("");
        let dataProduct = getData($(this));
        addToOrder(dataProduct).done(response => {
            if( parseInt(response) === 0)
            {
                swal({
                    icon: 'warning',
                    title: "Số lượng sản phẩm không đủ nữa rồi !!",
                })
            }
            getAllItem();
        });
    });
    body.on('click', '.btn-remove-item-cart', function () {
        let id = $(this).attr('data-id');
        let swal = messageDelete();
        let url = searchToBuy.attr('data-urlRemove');
        swal.then((willDelete) => {
            if (willDelete) {
                removeItemOrUpdate(id, url).done(response => {
                    getAllItem();
                });
            }
        });
    });
    body.on('change', '.quantity', function () {
        let id = $(this).attr('data-id');
        let qty = $(this).val();
        let data = {id: id, qty: qty};
        let url = searchToBuy.attr('data-urlUpdate');
        removeItemOrUpdate(data, url).done(response => {
            getAllItem();
        });
    });
    // customer
    function showInforCustomer(customer) {
        name.val(customer[0].name);
        email.val(customer[0].email);
        address.val(customer[0].address);
    }
    function showErrorCustomer(errors) {
        errors.name ? $('.name').text(errors.name[0]) : '';
        errors.email ? $('.email').text(errors.email[0]) : '';
        errors.address ? $('.address').text(errors.address[0]) : '';
        errors.phone ? $('.phone').text(errors.phone[0]) : '';
    }
    function resetValidate() {
        $('.name').text('');
        $('.email').text('');
        $('.address').text('');
        $('.phone').text('');
    }
    function resetInforCustomer() {
        name.val('');
        email.val('');
        address.val('');
    }
    phone.keyup(function () {
        let phone = $(this).val();
        let url = $(this).attr('data-url');
        phone = phone.replace(/[\D]/g, "");
        $(this).val(phone);
        if (phone.length === 10) {
            phone = {phone: phone};
            callApi(phone, url, 'GET')
                .done(response => {
                    if (response.status === 200) {
                        idCustomer = response.customer[0].id;
                        showInforCustomer(response.customer);
                    } else {
                        resetInforCustomer();
                    }
                })
        }

    });
    btnSaveNewCustomer.click(function (event) {
        event.preventDefault();
        let url = $(this).attr('data-url');
        let dataCustomer = $('#form-customer').serialize();
        callApi(dataCustomer, url, 'POST')
            .done(response => {
                swal(response.success_title, response.success_body, "success");
                idCustomer = response.id;
                resetValidate();
            })
            .fail(error => {
                let errors = error.responseJSON.errors;
                showErrorCustomer(errors);
            })

    });
    checkout.click(function () {
        if(idCustomer !== 0 ) {
            let url = '/admin/orders/check-out/'+idCustomer;
            window.location = url;
        }else{
            swal({
                icon: 'warning',
                title: "Ban chua nhap thong tin khach hang !!",
            })
        }

    })
});
