$(function () {
    let btnEditProduct = $('.btn-edit-product');
    let btnDeleteProduct = $('.btn-delete-product');
    let body = $('body');
    let btnDeleteMulti = $('#delete-multi');
    let quickSearch = $('#quick-search');
    let category = $('#category');
    let fromPrice = $('#from-price');
    let toPrice = $('#to-price');
    let btnSearch = $('#search-product');
    var isClick = false;

    function quickSearchProduct(page, query) {
        let urlQuickSearch = quickSearch.attr('data-url');
        searchAjax(page, urlQuickSearch, query)
            .done(response => {
                $('#table-product').html(response);
            })
    }

    function showErrorDataSearch(dataProduct) {
        let check = true;
        if (dataProduct["category"] === '') {
            $('.category').text("Ban chua chon danh muc");
            check = false;
        }

        if (dataProduct["fromPrice"] === '') {
            $('.from-price').text("Ban chua nhap gia");
            check = false;
        } else {
        }
        if (dataProduct["toPrice"] === '') {
            $('.to-price').text("Ban chua nhap gia");
            check = false;
        }
        return check;
    }

    function searchAjax_(dataProduct, page) {
        let url = btnSearch.attr("data-url");
        return $.ajax({
            url: url,
            type: "get",
            data: {
                category: dataProduct["category"],
                fromPrice: dataProduct["fromPrice"],
                toPrice: dataProduct["toPrice"],
                page: page,
            }
        });
    }

    function getDataSearchProduct() {
        let data_cate = category.val();
        let data_fromPrice = fromPrice.val();
        let data_toPrice = toPrice.val();
        let dataProduct = {category: data_cate, fromPrice: data_fromPrice, toPrice: data_toPrice};
        return dataProduct;
    }

    btnSearch.click(function () {
        let dataProduct = getDataSearchProduct();
        let check = showErrorDataSearch(dataProduct);
        if (check) {
            isClick = true;
            searchAjax_(dataProduct, 1)
                .done(response => {
                    $('#table-product').html(response);
                })
        }
    });

    category.click(function () {
        $('.category').text("");
        quickSearch.val("");
    });

    fromPrice.click(function () {
        $('.from-price').text("");
    });

    toPrice.click(function () {
        $('.to-price').text("");
    });

    fromPrice.keyup(function () {
        autoFormatPriceWhenInput(fromPrice);
    });

    toPrice.keyup(function () {
        autoFormatPriceWhenInput(toPrice);
    });

    btnDeleteProduct.click(function () {
        let form = $(this).parent();
        destroyConfirm(form);
    });

    body.on('click', '.check-all', function () {
        let statusCheck = $(this).prop('checked');
        $('.check-box').prop('checked', statusCheck);
    });

    btnDeleteMulti.click(function () {
        let tableCategory = $('#table-product');
        let id = [];
        $('.check-box:checked').each(function () {
            id.push($(this).val());
        });
        let products_id = JSON.stringify(id);
        let url = "/admin/products/multi-delete/" + products_id;
        deleteMulti(id, url, tableCategory);
    });

    quickSearch.click(function () {
        $('#category').val("");
        fromPrice.val("");
        toPrice.val("");
        isClick = false;
    });

    quickSearch.keyup(function () {
        let condition = $(this).val();
        quickSearchProduct(1, condition);
    });

    body.on('click', '.pagination li a', function (event) {
        event.preventDefault();
        let page = $(this).attr('href').split("=")[1];
        let data = quickSearch.val();
        let dataProduct = getDataSearchProduct();
        if (isClick) {
            searchAjax_(dataProduct, page).done(response => {
                $('#table-product').html(response);
            });
        } else {
            quickSearchProduct(page, data);
            isClick = false;
        }
    });
});
