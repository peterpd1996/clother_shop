$(function () {
    let btnDeleteCustoer = $('.btn-delete-customer');
    let btnSearch = $('#btnSearch');
    let body = $('body')
    let btnDeleteMulti = $('#delete-multi');
    function searchCustomer(page, condition) {
        let baseUrl = "/admin/customers/search";
        searchAjax(page, baseUrl, condition)
            .done(response => {
                if (response !== '') {
                    $('#table-customer').html(response);
                }
            });
    }
    btnDeleteCustoer.click(function (event) {
        let form = $(this).parent();
        destroyConfirm(form);
    });
    btnSearch.keyup(function () {
        let condition = $(this).val();
        searchCustomer(1, condition);
    });
    body.on('click', '.pagination li a', function (event) {
        event.preventDefault();
        let page = $(this).attr('href').split("=")[1];
        let query = $('#btnSearch').val();
        searchCustomer(page, query);
    });
    body.on('click', '.check-all', function () {
        let statusCheck = $(this).prop('checked');
        $('.check-box').prop('checked', statusCheck);
    });
    btnDeleteMulti.click(function () {
        let talbeCaterogy = $('#table-customer');
        let id = [];
        $('.check-box:checked').each(function () {
            id.push($(this).val());
        });
        let customers_id = JSON.stringify(id);
        let url = "/admin/customers/multi-delete/" + customers_id;
        deleteMulti(id, url, talbeCaterogy);
    });
});
