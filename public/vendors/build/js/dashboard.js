$(function () {
    let money = $('#money');
    let totalDay = parseInt(money.attr('data-totalDay'));
    let totalMonth = parseInt(money.attr('data-totalMonth'));
    dashboard(totalDay, totalMonth);
    function dashboard(totalDay, totalMonth) {
        let textDashboard = $('#text-dashboard');
        let saleDayAndMonth = textDashboard.attr('data-saleDayMonth');
        let orderTotal = textDashboard.attr('data-orderTotal');
        let saleDay = textDashboard.attr('data-saleDay');
        let saleMonth = textDashboard.attr('data-saleMonth');
        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: saleDayAndMonth
            },
            accessibility: {
                announceNewData: {
                    enabled: true
                }
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: ''
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:1f} VND'
                    }
                }
            },

            series: [
                {
                    name: orderTotal,
                    colorByPoint: true,
                    data: [
                        {
                            name: saleDay,
                            y: totalDay,
                            drilldown: saleDay,
                        },
                        {
                            name: saleMonth,
                            y: totalMonth,
                            drilldown: saleMonth
                        },
                    ]
                }
            ],
            drilldown: {
                series: [
                    {
                        name: "Chrome",
                        id: "Chrome",
                        data: [
                            [
                                "v65.0",
                                0.1
                            ],
                            [
                                "v64.0",
                                1.3
                            ],
                            [
                                "v63.0",
                                53.02
                            ],
                            [
                                "v62.0",
                                1.4
                            ],
                            [
                                "v61.0",
                                0.88
                            ],
                            [
                                "v60.0",
                                0.56
                            ],
                            [
                                "v59.0",
                                0.45
                            ],
                            [
                                "v58.0",
                                0.49
                            ],
                            [
                                "v57.0",
                                0.32
                            ],
                            [
                                "v56.0",
                                0.29
                            ],
                            [
                                "v55.0",
                                0.79
                            ],
                            [
                                "v54.0",
                                0.18
                            ],
                            [
                                "v51.0",
                                0.13
                            ],
                            [
                                "v49.0",
                                2.16
                            ],
                            [
                                "v48.0",
                                0.13
                            ],
                            [
                                "v47.0",
                                0.11
                            ],
                            [
                                "v43.0",
                                0.17
                            ],
                            [
                                "v29.0",
                                0.26
                            ]
                        ]
                    },
                    {
                        name: "Firefox",
                        id: "Firefox",
                        data: [
                            [
                                "v58.0",
                                1.02
                            ],
                            [
                                "v57.0",
                                7.36
                            ],
                            [
                                "v56.0",
                                0.35
                            ],
                            [
                                "v55.0",
                                0.11
                            ],
                            [
                                "v54.0",
                                0.1
                            ],
                            [
                                "v52.0",
                                0.95
                            ],
                            [
                                "v51.0",
                                0.15
                            ],
                            [
                                "v50.0",
                                0.1
                            ],
                            [
                                "v48.0",
                                0.31
                            ],
                            [
                                "v47.0",
                                0.12
                            ]
                        ]
                    },
                ]
            }
        });
    }
});
