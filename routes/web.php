<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/login','UserController@getLogin');
Route::post('/login','UserController@login')->name('login');
Route::get('/logout','UserController@logout')->name('logout');
Route::group(['prefix' => 'admin','middleware' => 'checkLogin'],function(){
    Route::get('/','DashboardController@index');
    Route::post('/lang','LanguageController@changeLang')->name('changeLang');

    Route::group(['prefix' => 'categories'],function(){
        Route::get('/','CategoryController@index')->name('admin.categories.index');
        Route::get('/create','CategoryController@create')->name('admin.categories.create');
        Route::post('/store','CategoryController@store')->name('admin.categories.store');
        Route::patch('/{id}','CategoryController@update')->name('admin.categories.update');
        Route::delete('/{id}','CategoryController@destroy')->name('admin.categories.destroy');
        Route::get('/search-pagination','CategoryController@search');
        Route::delete('/multi-delete/{categories_id}','CategoryController@multiDelete');
    });

    Route::group(['prefix' => 'customers'],function(){
        Route::get('/','CustomerController@index')->name('admin.customers.index');
        Route::get('/create','CustomerController@create')->name('admin.customers.create');
        Route::post('/store','CustomerController@store')->name('admin.customers.store');
        Route::get('/edit/{id}','CustomerController@edit')->name('admin.customers.edit');
        Route::patch('/{id}','CustomerController@update')->name('admin.customers.update');
        Route::get('/search','CustomerController@search');
        Route::delete('/{id}','CustomerController@destroy')->name('admin.customers.destroy');
        Route::delete('/multi-delete/{customers_id}','CustomerController@multiDelete');
        Route::get('/api-get-customer','CustomerController@getcustomer')->name('admin.customers');
        Route::post('/store-api','CustomerController@storeApi')->name('admin.customers.store_api');

    });

    Route::group(['prefix' => 'products'],function(){
        Route::get('/','ProductController@index')->name('admin.products.index');
        Route::get('/create','ProductController@create')->name('admin.products.create');
        Route::post('/store','ProductController@store')->name('admin.products.store');
        Route::get('/edit/{id}','ProductController@edit')->name('admin.products.edit');
        Route::patch('/{id}','ProductController@update')->name('admin.products.update');
        Route::get('/quick-search','ProductController@quickSearch')->name('admin.products.quick_search');
        Route::get('/search','ProductController@search')->name('admin.products.search');
        Route::delete('/{id}','ProductController@destroy')->name('admin.products.destroy');
        Route::delete('/multi-delete/{products_id}','ProductController@multiDelete');

    });

    Route::group(['prefix' => 'orders'],function(){
        Route::get('/','OrderController@index')->name('admin.orders.index');
        Route::get('/order-detail/{id}','OrderController@orderDetail')->name('admin.orders.order_detail');
        Route::get('/create','OrderController@create')->name('admin.orders.create');
        Route::post('/store','OrderController@store')->name('admin.orders.store');
        Route::get('/edit/{id}','OrderController@edit')->name('admin.orders.edit');
        Route::post('/add-item','OrderController@addItem')->name('admin.orders.add_item');
        Route::get('/search-to-buy','OrderController@getProduct')->name('admin.orders.get_product_to_by');
        Route::get('/get-all-items','OrderController@getAllItems')->name('admin.orders.get_all_items');
        Route::post('/remove-item','OrderController@removeItem')->name('admin.orders.remove_item');
        Route::post('/update-item','OrderController@updateItem')->name('admin.orders.update_item');
        Route::get('/check-out/{customer_id}','OrderController@checkOut')->name('admin.orders.checkout');
        Route::get('/pdf/{customer_id}','OrderController@printPdf')->name('admin.orders.pdf');
        Route::delete('/{id}','OrderController@destroy')->name('admin.orders.destroy');
    });

});
