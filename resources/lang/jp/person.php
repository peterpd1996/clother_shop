<?php
return [
    'customer' => 'お客様',
    'name' => '名前',
    'phone' => '電話',
    'phone_format' => '電話の形式が無効です, 正しい電話番号 0xxxxxxxxx',
    'address'=> '住所',
    'email' => 'Eメール',
];
