<?php
return [
    'order' => '順路', // so thu tu
    'id' => '識別',
    'add' => '追加',
    'edit'=> '編集する',
    'delete' => '削除する',
    'search' => '探す',
    'action' => 'アクション',
    'list'      => 'リスト',
    'look'      => '見て',
    'save'      => 'セーブ',
    'exit'      => '出口',
    'required'  => 'この値は必須です',
    'unique'   =>'この名前は存在します',
    'success_title'  => '成功 !',
    'success_add' => '正常に作成されました',
    'success_edit' => '正常に作成されました',
    'success_delete' => '正常に削除されました',
    'empty_check' => 'アイテムを選択していません',
    'confirm_delete' => '削除しますか ?',
    'multi_delete' => '複数削除',


];
