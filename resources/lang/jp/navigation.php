<?php
return [
    'welcome'   => 'ようこそ',
    'dashboard' => 'ダッシュボード',
    'category'  => 'カテゴリー',
    'add'       => '追加',
    'list'      => 'リスト',
    'product' => '製品',
];
