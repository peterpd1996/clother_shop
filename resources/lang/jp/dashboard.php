<?php
return [
    'total_customers' => '総顧客',
    'total_products' => 'トータル製品',
    'total_orders' => '合計注文',
    'total_categories' => '合計カテゴリ',
    'sale_day_month' => '日別および月別の売上',
    'sale_day' => '毎日の販売',
    'sale_month' => '月間売上',
    'top_product' => 'トップ の製品ホット',
];
