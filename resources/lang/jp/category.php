<?php
return [
   'category_name' =>'カテゴリ名',
    'category_title'=>'題名',
    'category'  => 'カテゴリー',
    'add_new_cate' => '新しいカテゴリを追加',
    'edit_cate' => 'カテゴリーを編集',
    'category_slug' =>'URLパス',
    'select' => 'カテゴリを選んでください'
];
