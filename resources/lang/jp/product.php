<?php
return [

    'name' => '商品名',
    'image' => '画像',
    'information' => '情報',
    'quick_search' => 'クイック検索',
    'from_price' => '価格から',
    'to_price' => '価格に',
    'quantity' => '量',
    'description' => '説明',
    'sale' => 'セール',
    'price' => '価格',
    'title'=>'題名',
    'slug' =>'URLパス',
    'not_available' => '利用可能な製品はありません !!'


];
