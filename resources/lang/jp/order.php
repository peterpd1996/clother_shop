<?php
return [
    'bill' => '請求書',
    'invoice' => '請求書',
    'sale_day' => '発売日',
    'total_money' => '合計金額',
    'bill_information' => '請求書情報',
    'money_product' =>'合計金額',
    'employee' => '社員',
    'sale_management'=> '販売管理',
    'checkout'=> 'チェックアウト',
    'search' => 'タイプ名またはID',
     'best_place_provide' => 'スマートフォンを提供するのに最適な場所',
    'total' => '合計',
    'print_pdf' => '印刷する PDF',
    'thank_u' => 'ありがとうございました',
    'real_price' => '実質価格',
    'total_quantity_sold' => '総販売数',
];
