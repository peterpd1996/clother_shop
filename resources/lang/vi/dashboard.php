<?php
return [
    'total_customters' => 'Tổng số khách hàng',
    'total_products' => 'Tổng số sản phẩm',
    'total_orders' => ' Tổng số hóa đơn',
    'total_categories' => 'Tổng số danh mục',
    'sale_day_month' => 'Doanh số bán hàng theo ngày và theo tháng ',
    'sale_day' => 'Doanh thu ngày ',
    'sale_month' => 'Doanh thu tháng ',
    'top_product' => 'Top sản phẩm bán chạy',
];
