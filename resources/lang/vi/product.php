<?php
return [
    'name' => 'Tên sản phẩm',
    'image' => 'Hình ảnh',
     'information' => 'Thông tin chung',
    'quick_search' => 'Tìm kiếm nhanh',
    'from_price' => 'Giá từ',
    'to_price' => 'Đến giá',
    'quantity' => 'Số lượng',
    'description' => 'Mô tả',
    'sale' => 'Giảm giá',
    'price' => 'Giá',
    'slug' =>'Đường dẫn',
    'title'=>'Tiêu đề',
    'not_available' => 'Không có sản phẩm nào như bạn tìm cả !!'

];
