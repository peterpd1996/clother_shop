<?php
return [
    'customer' => 'Khách hàng',
    'name' => 'Họ và tên',
    'phone' => 'Số điện thoại',
    'phone_format' => 'Số điện thoại k hợp lệ, số chuẩn 0xxxxxxxxx',
    'address'=> 'Địa chỉ',
    'email' => 'Email',
];
