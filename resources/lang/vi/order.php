<?php
return [
    'bill' => 'Hóa đơn',
    'invoice' => 'Hóa đơn báng hàng',
    'sale_day' => 'Ngày bán',
    'total_money' => 'Thành tiền',
    'bill_information' => 'THÔNG TIN THANH TOÁN',
    'money_product' =>'Tiền hàng',
    'employee' => 'Nhân viên',
    'sale_management'=> 'QUẢN LÝ BÁN HÀNG',
    'checkout'=> 'Thanh toán',
    'search' => 'Nhập tên hoặc sản phẩm',
    'best_place_provide' => 'Chuyên cung cấp điện thoại tốt nhất',
    'total' => 'Tông tiền ',
    'print_pdf' => 'Xuất PDF',
    'thank_u' => 'Cám ơn quý khách đã mua hàng của chúng tôi !!',
    'real_price' => 'Giá bán',
    'total_quantity_sold' => 'Số lượng bán',

];
