<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <img
                            src="https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwi13srI9-TmAhV94jgGHdCvAowQjRx6BAgBEAQ&url=http%3A%2F%2Ft0.gstatic.com%2Fimages%3Fq%3Dtbn%3AANd9GcTjlX5DXmAyO_sSC13eEqX_2La65AJ-LHf76X2veuvrDeNAmWmr&psig=AOvVaw25CjcYnhwRYwI9Jc0JouCC&ust=1578055119726232"
                            alt="">
                        @if(Auth::check())
                            {{Auth::user()->name}}
                            <span class=" fa fa-angle-down"></span>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li><a href="javascript:"> {{ trans('header.profile')  }}</a></li>
                        <li><a href="{{ route('logout') }}"><i class="fa fa-sign-out pull-right"></i>{{ trans('header.log_out') }}
                            </a></li>
                    </ul>
                    @endif
                </li>
                <li style="line-height: 60px">
                    <form action="{{ route('changeLang') }}" class="form-lang" method="post">
                        <select name="locale" onchange='this.form.submit();'>
                            <option
                                value="vi" {{ Lang::locale() === 'vi' ? 'selected' : '' }}>{{ trans('header.lang_vi') }}</option>
                            <option
                                value="jp" {{ Lang::locale() === 'jp' ? 'selected' : '' }}>{{ trans('header.lang_jp')  }}</option>
                        </select>
                        {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </nav>
    </div>
</div>
