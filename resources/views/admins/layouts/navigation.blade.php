<div class="col-md-3 left_col" style="font-size: 16px">
  <div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
      <a href="/admin" class="site_title"><i class="fa fa-paw"></i> <span>D'look</span></a>
    </div>

    <div class="clearfix"></div>

    <!-- menu profile quick info -->
    <div class="profile clearfix">
      <div class="profile_pic">
        <img src="https://www.google.com/url?sa=i&source=images&cd=&ved=2ahUKEwi13srI9-TmAhV94jgGHdCvAowQjRx6BAgBEAQ&url=http%3A%2F%2Ft0.gstatic.com%2Fimages%3Fq%3Dtbn%3AANd9GcTjlX5DXmAyO_sSC13eEqX_2La65AJ-LHf76X2veuvrDeNAmWmr&psig=AOvVaw25CjcYnhwRYwI9Jc0JouCC&ust=1578055119726232" alt="..." class="img-circle profile_img">
      </div>
      <div class="profile_info">
        <span>{{ trans('navigation.welcome') }},</span>
        <h2>{{ Auth::user()->name }}</h2>
      </div>
    </div>
    <!-- /menu profile quick info -->

    <br />

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <div class="menu_section">
        <ul class="nav side-menu">
          <li><a href="/admin"><i class="fa fa-home"></i> {{ trans('navigation.dashboard')  }} </a></li>
          <li><a><i class="fa fa-edit"></i> {{ trans('navigation.category')  }} <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="{{ route('admin.categories.index') }}">{{ trans('navigation.list')  }}</a></li>
            </ul>
          </li>
          <li><a><i style="font-size: 27px" class="fa fa-mobile-phone"></i>{{ trans('navigation.product') }} <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="{{ route('admin.products.index') }}">{{ trans('usually.list') }}</a></li>
              <li><a href="{{ route('admin.products.create') }}">{{ trans('usually.add') }}</a></li>
            </ul>
          </li>
          <li><a><i class="fa fa-user"></i> {{ trans('person.customer') }}<span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="{{ route('admin.customers.index') }}">{{ trans('usually.list') }}</a></li>
              <li><a href="{{ route('admin.customers.create') }}">{{ trans('usually.add') }}</a></li>
            </ul>
          </li>
          <li><a><i class="fa fa-bar-chart-o"></i> {{ trans('order.bill') }} <span class="fa fa-chevron-down"></span></a>
            <ul class="nav child_menu">
              <li><a href="{{ route('admin.orders.create') }}">{{ trans('usually.add') }} </a></li>
              <li><a href="{{ route('admin.orders.index') }}">{{ trans('usually.list') }}</a></li>
            </ul>
          </li>
        </ul>
      </div>


    </div>
    <!-- /sidebar menu -->

    <!-- /menu footer buttons -->
    <div class="sidebar-footer hidden-small">
      <a data-toggle="tooltip" data-placement="top" title="Settings">
        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="FullScreen">
        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Lock">
        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
      </a>
    </div>
    <!-- /menu footer buttons -->
  </div>
</div>
