@extends('admins.layouts.app')
@section('content')
    <div class="row tile_count">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
                        <span class="count_top"><i class="fa fa-user"></i> &nbsp;{{ trans('dashboard.total_customters') }}</span>
                        <div class="count green">{{ $totalCustomers }}</div>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
                        <span class="count_top"><i class="fa fa-clock-o"></i>&nbsp;{{ trans('dashboard.total_products') }}</span>
                        <div class="count green">{{ $totalProducts }}</div>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
                        <span class="count_top"><i class="fa fa-user"></i> &nbsp; {{ trans('dashboard.total_categories') }}</span>
                        <div class="count green">{{$totalCategories}}</div>
                    </div>
                    <div class="col-md-3 col-sm-4 col-xs-6 tile_stats_count">
                        <span class="count_top"><i class="fa fa-user"></i> {{ trans('dashboard.total_orders') }}</span>
                        <div class="count green">{{ $totalOrders }}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
        </div>
    </div>
    <div class="row">
        <input type="hidden" id="money" data-totalDay="{{ $totalDay }}" data-totalMonth="{{ $totalMonth }}">
        <input type="hidden" id="text-dashboard" data-saleDayMonth="{{ trans('dashboard.sale_day_month') }}"
               data-orderTotal="{{  trans('order.total') }}" data-saleDay="{{ trans('dashboard.sale_day') }}"
               data-saleMonth="{{ trans('dashboard.sale_month') }}">
        <div class="col-md-5" style="margin-top: 45px">
            <figure class="highcharts-figure">
                <div id="container"></div>
            </figure>
        </div>
        <div class="col md-7">
            <div class="table-responsive" id="table-category">
                <h3 style="text-align: center">{{ trans('dashboard.top_product') }} </h3>
                <table class="table table-striped jambo_table bulk_action">
                    <thead>
                    <tr class="headings">
                        <th class="column-title">{{ trans('usually.order') }}</th>
                        <th class="column-title">{{ trans('product.name') }}</th>
                        <th class="column-title">{{ trans('product.price') }}</th>
                        <th class="column-title">{{ trans('product.sale') }}</th>
                        <th class="column-title">{{ trans('order.real_price') }}</th>
                        <th class="column-title">{{ trans('order.total_quantity_sold') }}</th>

                    </tr>
                    </thead>
                    <tbody id="tbody">
                    @php($stt = 1)
                    @foreach($productsHot as $product)
                        <tr class="even pointer">
                            <td>{{ $stt++ }}</td>
                            <td>{{ $product->name }}</td>
                            <td>{{ formatPrice($product->price)}}</td>
                            <td><span class="label label-success">{{ $product->sale ?? 0 }} %</span></td>
                            <td>{{ formatPriceSale($product->price,$product->sale) }}</td>
                            <td>{{ $product->total }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('vendors/build/js/dashboard.js') }}"></script>
@endsection
