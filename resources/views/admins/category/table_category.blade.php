<table class="table table-striped jambo_table bulk_action" >
    <thead>
    <tr class="headings">
        <th >
            <input type="checkbox" class="check-all">
        </th>
        <th class="column-title">{{ trans('usually.order') }}</th>
        <th class="column-title">{{ trans('category.category_name') }}</th>
        <th class="column-title">{{ trans('category.category_slug') }}</th>
        <th class="column-title">{{ trans('category.category_title')  }}</th>
        <th class="column-title">{{ trans('usually.action') }}</th>
    </tr>
    </thead>
    <tbody id="tbody">
    <?php $stt = $categories->perPage() * ($categories->currentPage() - 1); $stt++; ?>
    @foreach($categories as $category)
        <tr class="even pointer cate_{{$category->id}}">
            <td>
                <input type="checkbox" value="{{$category->id}}" class="check-box">
            </td>
            <td class="order">{{ $stt++ }}</td>
            <td class="namecate">{{ $category->name }}</td>
            <td>{{ $category->slug }}</td>
            <td class="titleCate">{{ $category->title }}</td>
            <td>
                <button class="btn btn-info btn-xs btnEditCate" data-target="#editCateModal"
                        data-toggle="modal" data-id="{{$category->id}}"
                        data-name="{{ $category->name }}" data-title="{{$category->title}}"><i
                        class="fa fa-pencil"></i> {{ trans('usually.edit') }} </button>
                <button href="#" class="btn btn-danger btn-xs btnDeleteCate"
                        data-id="{{$category->id}}"><i
                        class=" fa fa-trash-o"></i> {{trans('usually.delete')}} </button>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
 <div>{{ $categories->links() }}</div>













