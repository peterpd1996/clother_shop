<div class="modal fade" id="addCateModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="modal-title" id="exampleModalLabel">{{ trans('category.add_new_cate') }}</h3>
            </div>
            <div class="modal-body">
                <form id="newResourceCategoryForm">
                    @csrf
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">{{ trans('category.category_name') }}</label>
                        <input type="text" class="form-control"  name="name" id="nameError">
                    </div>
                    <div class="error nameError" ></div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">{{ trans('category.category_title') }}</label>
                        <input class="form-control" name="title" id="titleError">
                    </div>
                    <div class="titleError error"></div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success"
                                id="saveNewCate">{{ trans('usually.save') }}</button>
                        <button type="button" class="btn btn-secondary"
                                data-dismiss="modal">{{ trans('usually.exit') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
