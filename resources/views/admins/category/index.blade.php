@extends('admins.layouts.app')
@section('content')
    <!--modal add -->
    @include('admins.category.add_modal')
    @include('admins.category.edit_modal')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <nav aria-label="breadcrumb" style="font-size: 17px">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page">{{ trans('category.category') }}</li>
                        <li class="breadcrumb-item " aria-current="page">{{ trans('usually.list') }}</li>
                    </ol>
                </nav>
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <input style="border-radius: 18px" type="text" class="form-control search"
                                   placeholder="{{ trans('usually.search') }} ..."
                                   size="40" id="inputSearch">
                        </div>
                    </div>
                    <div class="col-md-3 col-md-offset-1 ">
                        <button class="btn btn-success" data-toggle="modal" data-target="#addCateModal"
                                id="btn-add-new"><i
                                class="fa fa-plus-circle"></i> {{ trans('usually.add') }}</button>
                    </div>
                </div>
                <div class="row">
                    <button style="margin-left: 11px;" class="btn btn-danger" id="delete-multi"><i
                            class="fa fa-trash-o"></i> {{ trans('usually.multi_delete') }}</button>
                </div>
                <div class="row">
                    <div class="col-md-8 col-sm-12 col-xs-12">
                        <div class="table-responsive" id="table-category">
                            @include('admins.category.table_category')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
@section('script')
    <script src="{{ asset('vendors/build/js/category.js') }}"></script>
@endsection

