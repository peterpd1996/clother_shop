@extends('admins.layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <nav aria-label="breadcrumb" style="font-size: 17px">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page">{{ trans('person.customer') }}</li>
                            <li class="breadcrumb-item " aria-current="page"><a
                                    href="{{ route('admin.customers.index') }}">{{ trans('usually.list') }}</a></li>
                            <li class="breadcrumb-item " aria-current="page">{{ trans('usually.add') }}</li>
                        </ol>
                    </nav>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form method="post" action="{{ route('admin.customers.store') }}"
                          class="form-horizontal form-label-left">
                        @csrf
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">{{ trans('person.name') }}<span
                                    class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="name"
                                       class="form-control col-md-7 col-xs-12 parsley-success"
                                       value="{{ old('name') }}">
                            </div>
                            @error('name')
                            <span class="error">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('person.phone') }}<span
                                    class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="phone"
                                       class="form-control col-md-7 col-xs-12 parsley-success"
                                       value="{{ old('phone') }}">
                            </div>
                            @error('phone')
                            <span class="error">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">{{ trans('person.email') }}<span
                                    class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="email" name="email"
                                       class="form-control col-md-7 col-xs-12 parsley-success"
                                       value="{{ old('email') }}">
                            </div>
                            @error('email')
                            <span class="error">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">{{ trans('person.address') }}<span
                                    class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="address"
                                       class="form-control col-md-7 col-xs-12 parsley-success"
                                       value="{{ old('address')}}">
                            </div>
                            @error('address')
                            <span class="error">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" id="btn-save-new-customer" class="btn btn-success">{{ trans('usually.add') }}</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $("input").keyup(function (e) {
            if(e.keyCode === 13){
                $('#btn-save-new-customer').click();
            }
        })
    </script>
@stop
