<table class="table table-striped jambo_table bulk_action">
    <thead>
    <tr class="headings">
        <th >
            <input type="checkbox" class="check-all">
        </th>
        <th class="column-title">{{ trans('usually.order') }}</th>
        <th class="column-title">{{ trans('person.name') }}</th>
        <th class="column-title">{{ trans('person.phone') }}</th>
        <th class="column-title">{{ trans('person.email') }}</th>
        <th class="column-title">{{ trans('person.address') }}</th>
        <th class="column-title">{{ trans('usually.action') }}</th>
    </tr>
    </thead>
    <tbody>
    <?php $stt = $customers->perPage() * ($customers->currentPage() - 1); $stt++; ?>
    @foreach($customers as $customer)
        <tr class="even pointer ">
            <td>
                <input type="checkbox" value="{{$customer->id}}" class="check-box">
            </td>
            <td>{{ $stt++ }}</td>
            <td>{{ $customer->name }}</td>
            <td>{{ $customer->phone }}</td>
            <td>{{ $customer->email }}</td>
            <td>{{ $customer->address }}</td>
            <td>
                <a href="{{route('admin.customers.edit',['id'=>$customer->id])}}"
                   class="btn btn-info btn-xs btn-edit-customer"><i
                        class="fa fa-pencil"></i> {{ trans('usually.edit') }} </a>
                <form style="display: inline" action="{{route('admin.customers.destroy',['id'=>$customer->id])}}" method="post">
                    @csrf
                    @method('DELETE')
                    <div class="btn btn-danger btn-xs btn-delete-customer"><i
                            class=" fa fa-trash-o"></i> {{ trans('usually.delete') }} </div>
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<div>{{ $customers->links() }}</div>
