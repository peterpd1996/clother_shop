@extends('admins.layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <nav aria-label="breadcrumb" style="font-size: 17px">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page">{{ trans('person.customer') }}</li>
                            <li class="breadcrumb-item " aria-current="page">{{ trans('usually.list') }}</li>
                        </ol>
                    </nav>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-11">
                            <div class="input-group">
                                <input style="border-radius: 18px" type="text" class="form-control search"
                                       placeholder="{{ trans('usually.search') }} ..."
                                       size="40" id="btnSearch">
                            </div>
                        </div>
                        <div class="col-md-1">
                            <a href="{{ route('admin.customers.create') }}" class="btn btn-success"><i
                                    class="fa fa-plus-circle"></i> {{ trans('usually.add') }}</a>
                        </div>
                    </div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <button class="btn btn-danger" id="delete-multi"
                                data-message="{{ trans('usually.success_delete')}}"
                                data-message="{{ trans('usually.success_delete')}}"
                                data-empty="{{ trans('usually.empty_check') }}"><i
                                class="fa fa-trash-o"></i> {{ trans('usually.multi_delete') }}</button>
                    </div>
                    <div class="row" id="table-customer">
                        @include('admins.customers.table_customer')
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('vendors/build/js/customer.js') }}"></script>
@stop

