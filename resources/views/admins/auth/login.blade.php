<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentelella Alela! | </title>

    <!-- Bootstrap -->
    <link href="{{ asset('vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="{{ asset('vendors/nprogress/nprogress.css') }}" rel="stylesheet">
    <!-- NProgress -->
    <
    <!-- Animate.css -->
    <!-- Custom Theme Style -->
    <link href="{{ asset('vendors/build/css/custom.min.css') }}" rel="stylesheet">
    <style>
        .error{
            color: red;
        }
    </style>
</head>

<body class="login">
<div>
    <a class="hiddenanchor" id="signup"></a>
    <a class="hiddenanchor" id="signin"></a>

    <div class="login_wrapper">
        <div class="animate form login_form">
            <section class="login_content">
                <form action="{{ route('login') }}" method="post">
                    @csrf
                    <h1>Login Form</h1>
                    <div>
                        <input type="text" class="form-control" name="email" placeholder="Email"  autocomplete="off"  value="{{ old('email') }}" />
                    </div>
                    @error('email')
                    <span class="error">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                    <div>
                        <input type="password" class="form-control" name="password" placeholder="Password" value="{{ old('password') }}  autocomplete="off"/>
                    </div>
                    @error('password')
                    <span class="error">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                    @if(session('message'))
                        <div class="alert alert-danger " role="alert">
                            <span>{{ session('message') }} <i class="fa fa-check"></i></span>
                        </div>

                    @endif
                    <div>
                        <button class="btn btn-success" type="submit">Log in</button>
                        <a class="reset_pass" href="#">Lost your password?</a>
                    </div>

                    <div class="clearfix"></div>

                    <div class="separator">
                        <p class="change_link">New to site?
                            <a href="#signup" class="to_register"> Create Account </a>
                        </p>

                        <div class="clearfix"></div>
                        <br />

                    </div>
                </form>
            </section>
        </div>

    </div>
</div>
</body>
</html>
