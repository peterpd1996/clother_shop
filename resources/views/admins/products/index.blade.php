@extends('admins.layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <nav aria-label="breadcrumb" style="font-size: 17px">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active"
                                aria-current="page">{{ trans('navigation.product') }}</li>
                            <li class="breadcrumb-item " aria-current="page">{{ trans('usually.list') }}</li>
                        </ol>
                    </nav>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="input-group">
                                <input style="border-radius: 18px" type="text" class="form-control search"
                                       placeholder="{{ trans('product.quick_search') }} ..."
                                       size="40" id="quick-search" data-url="{{route('admin.products.quick_search')}}">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="form-group  col-md-3">
                                <select id="category" name="category" class="form-control">
                                    <option value="">-- {{ trans('category.category_name') }} --</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                                <span class="error category">
                                </span>
                            </div>
                            <div class="form-group  col-md-3">
                                <input type="text" name="fromPrice" id="from-price" class="form-control"
                                       placeholder="{{ trans('product.from_price') }}">
                                <span class="error from-price">
                                </span>
                            </div>
                            <div class="form-grou  col-md-3">
                                <input type="text" name="toPrice" class="form-control"
                                       placeholder="{{ trans('product.to_price') }}"
                                       id="to-price">
                                <span class="error to-price">
                                </span>
                            </div>
                            <div class="col-md-1">
                                <div style="cursor: pointer" class="fa fa-search "
                                     id="search-product" aria-hidden="true"
                                     data-url="{{ route('admin.products.search') }}"></div>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <a href="{{ route('admin.products.create') }}" class="btn btn-success"><i
                                    class="fa fa-plus-circle"></i> {{ trans('usually.add') }}</a>
                        </div>
                    </div>
                </div>
                <div class="x_content">
                    <div class="row">
                        <button class="btn btn-danger" id="delete-multi"><i
                                class="fa fa-trash-o"></i> {{ trans('usually.multi_delete') }}
                        </button>
                    </div>
                    <div class="row" id="table-product">
                        @include('admins.products.table_product');
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
@section('script')
    <script src="{{ asset('vendors/build/js/product.js') }}"></script>
@stop
