@extends('admins.layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <nav aria-label="breadcrumb" style="font-size: 17px">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page"> {{ trans('navigation.product') }}</li>
                            <li class="breadcrumb-item " aria-current="page"><a
                                    href="{{ route('admin.products.index') }}">{{ trans('usually.list') }}</a></li>
                            <li class="breadcrumb-item " aria-current="page">{{ trans('usually.add') }}</li>
                        </ol>
                    </nav>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <form method="post" action="{{ route('admin.products.store') }}" enctype="multipart/form-data"
                          class="form-horizontal form-label-left">
                        @csrf
                        <div class="form-group row">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> {{ trans('product.name') }}
                                <span
                                    class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="name"
                                       class="form-control col-md-7 col-xs-12 parsley-success"
                                       value="{{ old('name') }}"  @error('name') autofocus @enderror >
                                @error('name')
                                <span class="error">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">{{ trans('navigation.category') }}<span
                                    class="required">*</span></label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select name="category_id" class="form-control">
                                    <option value="">--{{ trans('category.select') }}--</option>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                                @error('category_id')
                                <span class="error">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">{{ trans('product.image') }}<span
                                    class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div style="position: relative" class="ml-5">
                                    <div id="icon-upload"><i class="fa fa-picture-o" aria-hidden="true"></i> Photo</div>
                                    <input id="upload-image" type="file" name="image"
                                           class="" autocomplete="image" >
                                </div>
                                @error('image')
                                <span class="error">
                                  <strong>{{ $message }}</strong>
                                 </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row form-group">
                            <label class="col-md-3"></label>
                            <img id="img_output" style="margin-left: 12px" width="200px" height="200px"
                                 src=" {{ asset('images/image-not-available.jpg') }}" alt="">
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">{{ trans('product.price') }}<span
                                    class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="price"
                                       class="form-control  parsley-success"
                                       value="{{ old('price')}}" @error('price') autofocus @enderror id="price" placeholder="VND">

                                @error('price')
                                <span class="error">
                                  <strong>{{ $message }}</strong>
                                 </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">{{ trans('product.quantity') }} <span
                                    class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" name="quantity"
                                       class="form-control  parsley-success"
                                       value="{{ old('quantity')}}" @error('quantity') autofocus @enderror >
                                @error('quantity')
                                <span class="error">
                                  <strong>{{ $message }}</strong>
                                 </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">% {{ trans('product.sale') }}
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" name="sale"
                                       class="form-control  parsley-success"
                                       value="{{ old('sale')}}" min="0" max="100">

                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name"> {{ trans('product.title') }}<span
                                    class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" name="title"
                                       class="form-control  parsley-success"
                                       value="{{ old('title')}}" @error('title') autofocus @enderror>

                                @error('title')
                                <span class="error">
                                <strong>{{ $message }}</strong>
                            </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">{{ trans('product.description') }}<span
                                    class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea class="form-control ckeditor" rows="7" name="description"
                                          id="demo" @error('description') autofocus @enderror ></textarea>

                            @error('description')
                            <span class="error">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" id="btn-save-new-product"
                                        class="btn btn-success">{{ trans('usually.add') }}</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        $(function () {
            let inputPrice = $("#price");
            let inputElement = $('input');
            inputPrice.on("keyup", function (event) {
                autoFormatPriceWhenInput(inputPrice);
            });
            inputElement.keyup(function (event) {
                if(event.keyCode === 13){
                    $('#btn-save-new-product').click();
                }
            })
        });
    </script>
@stop
