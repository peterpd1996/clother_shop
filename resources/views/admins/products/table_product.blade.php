<table class="table table-striped jambo_table bulk_action">
    <thead>
    <tr class="headings">
        <th>
            <input type="checkbox" class="check-all">
        </th>
        <th class="column-title">{{ trans('usually.order') }}</th>
        <th class="column-title">{{ trans('product.name') }}</th>
        <th class="column-title">{{ trans('product.image') }}</th>
        <th class="column-title">{{ trans('product.title') }}</th>
        <th class="column-title">{{ trans('product.slug') }}</th>
        <th class="column-title">{{ trans('product.information') }}</th>
        <th class="column-title">{{ trans('usually.action') }}</th>
    </tr>
    </thead>
    <tbody>
    <?php $stt = $products->perPage() * ($products->currentPage() - 1); $stt++; ?>
    @foreach($products as $product)
        <tr class="even pointer ">
            <td>
                <input type="checkbox" value="{{ $product->id }}" class="check-box">
            </td>
            <td>{{ $stt++ }}</td>
            <td>{{ $product->name }}</td>
            <td><img width="100px" height="100px" src="/images/products/{{ $product->image }}"
                     alt=""></td>
            <td>{{ $product->title }}</td>
            <td>{{ $product->slug }}</td>
            <td>
                <div>{{ trans('category.category') }} : <span
                        class="label label-success">{{ $product->category->name }}</span>
                </div>
                <div>{{ trans('product.quantity') }}: <strong>{{ $product->quantity }}</strong></div>
                <div>{{ trans('product.price') }}: <strong>{{ formatPrice($product->price) }}</strong></div>
                @if($product->sale != null)
                    <div>{{ trans('product.sale') }}: <strong>{{ $product->sale }}%</strong> |
                        <strong>{{ formatPriceSale($product->price,$product->sale) }}</strong>
                    </div>
                @endif
            </td>
            <td >
                <a href="{{route('admin.products.edit',['id'=>$product->id])}}"
                   class="btn btn-info btn-xs btn-edit-product"><i
                        class="fa fa-pencil"></i> {{ trans('usually.edit') }} </a>
                <form style="display: inline" action="{{route('admin.products.destroy',['id'=>$product->id])}}" method="post">
                    @csrf
                    @method('DELETE')
                    <div class="btn btn-danger btn-xs btn-delete-product"><i
                            class=" fa fa-trash-o"></i> {{ trans('usually.delete') }} </div>
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<div>{{ $products->links() }}</div>
