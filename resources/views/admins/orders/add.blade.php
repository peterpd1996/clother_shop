@extends('admins.layouts.app')
@section('content')
    <div class="row" style="margin-top: 61px">
        <div class="alert alert-success alert-dismissible" role="alert">
            <span><i class="fa fa-cart-arrow-down"
                     style="font-size: 22px"></i>   {{trans('order.sale_management')}} </span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 ">
            <div class="x_panel">
                <div class="titlle">
                    <div class="row">
                        <div class="col-md-7">
                            <div class="input-group" id="buy-prodct" style="position: relative">
                                <input style="border-radius: 18px" type="text" class="form-control search"
                                       autocomplete="off"
                                       placeholder="{{ trans('order.search') }} ..."
                                       size="80" id="search-to-buy"
                                       data-url="{{route('admin.orders.get_product_to_by')}}"
                                       data-urlAdd="{{ route('admin.orders.add_item') }}"
                                       data-urlGetAll="{{ route('admin.orders.get_all_items') }}"
                                       data-urlRemove="{{ route('admin.orders.remove_item') }}"
                                       data-urlUpdate="{{ route('admin.orders.update_item') }}"
                                >
                                <div class="product-search"
                                     style="position: absolute;max-height: 500px;overflow-y: auto"
                                     id="product-get-by-search">
                                    {{--      here result product by search--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <div class="row" id="table-product">
                        <div class="col-md-12" id="order-table">
                            <table class="table table-striped jambo_table bulk_action">
                                <thead>
                                <tr class="headings">

                                    <th class="column-title">{{ trans('usually.id') }}</th>
                                    <th class="column-title">{{ trans('product.image') }}</th>
                                    <th class="column-title">{{ trans('product.name') }}</th>
                                    <th class="column-title">{{ trans('product.quantity') }}</th>
                                    <th class="column-title">{{ trans('order.total_money') }}</th>
                                    <th class="column-title">{{ trans('product.price') }}</th>
                                    <th class="column-title">{{ trans('usually.action') }}</th>
                                </tr>
                                </thead>
                                <tbody id="tbody">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 infor">
            <div class="x_panel">
                <div style="padding: 17px 0px">
                    <span style="text-transform: uppercase"><i class="fa fa-info-circle iconf"></i> {{ trans('person.customer') }} </span>
                </div>
                <form id="form-customer">
                    <div class="form-group row">
                        <label class="control-label col-md-4 col-sm-3 col-xs-12">{{ trans('person.name') }} * :</label>
                        <div class="col-md-7 col-sm-6 col-xs-12">
                            <input autocomplete="off" type="text" name="name"
                                   class="form-control col-md-6 col-xs-12" id="name" value="">
                            <div class="error name"></div>
                        </div>
                        <div class="col-md-1">
                            <button type="submit" class="btn btn-success" id="save-new-customer"
                                    data-url="{{route('admin.customers.store_api')}}"><i
                                    class="  fa fa-plus-circle"></i></button>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-4 col-sm-3 col-xs-12">{{ trans('person.phone') }} *:</label>
                        <div class="col-md-7 col-sm-6 col-xs-12">
                            <input autocomplete="off" type="text"
                                   class="form-control col-md-6 col-xs-12"
                                   name="phone" id="phone" data-url="{{ route('admin.customers') }}">
                            <div class="error phone"></div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-4 col-sm-3 col-xs-12">{{ trans('person.address') }} *
                            :</label>
                        <div class="col-md-7 col-sm-6 col-xs-12">
                            <input type="text" id="address"
                                   class="form-control col-md-6 col-xs-12 "
                                   value="" autocomplete="off"
                                   name="address"
                            >
                            <div class="error address"></div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="control-label col-md-4 col-sm-3 col-xs-12">{{ trans('person.email') }} * :</label>
                        <div class="col-md-7 col-sm-6 col-xs-12">
                            <input type="email" id="email"
                                   class="form-control col-md-6 col-xs-12 "
                                   name="email">
                            <div class="error email"></div>
                        </div>
                    </div>
                </form>
                <div class="row" style="margin-top: 40px">
                    </form>
                    <div style="padding: 17px 0px">
                        <span style="text-transform: uppercase;"><i class="fa fa-info-circle iconf"></i> {{ trans('order.bill_information') }}</span>
                    </div>
                </div>
                <div class="row">
                    <label class="control-label col-md-4 col-sm-3 col-xs-12">{{ trans('order.total_money') }} :</label>
                    <div class="col-md-7" id="price">0 đ</div>
                </div>
                <div class="row">
                    <label class="control-label col-md-4 col-sm-3 col-xs-12">{{ trans('order.employee') }} :</label>
                    <div class="col-md-7">{{ Auth::user()->name }}</div>
                </div>
                <div class="row" style="text-align: center;">
                    <div class="  btn btn-success" style="margin-top: 9px;padding: 11px 148px;font-size: 20px;"
                         id="check-out">
                        <i class="fa fa-check" aria-hidden="true"></i>
                        {{ trans('order.checkout') }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script src="{{ asset('vendors/build/js/order.js') }}"></script>
@endsection


