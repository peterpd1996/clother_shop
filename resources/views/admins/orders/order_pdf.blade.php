<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <style>
        .border{
            border: 1px solid;
            padding:12px;
        }
    </style>
</head>
<body style="text-align: center ;font-family:{{ Lang::locale() === 'jp' ? 'ipag' : 'DejaVu Sans' }};width: 730px;margin: 0 auto">
<table  style="margin: 0 auto;width: 100%">
    <tr>
        <td style="width: 50%;">
            <div >
                <div><h1>D'shop</h1></div>
                <div style="font-style: italic">{{ trans('order.best_place_provide') }}</div>
            </div>
        </td>
        <td style="width: 50%">
            <div >
                <div>{{ trans('person.phone') }}: 33052100</div>
                <div>{{ trans('person.address') }}:88, lai xá, kim chung</div>
                <div>Facebook: facebook/phone</div>
            </div>
        </td>
    </tr>
</table>
<h1 style="text-transform: uppercase">{{ trans('order.invoice') }}</h1>
<div style="padding-bottom: 10px">{{ trans('order.sale_day') }} : {{ date('d-m-Y') }}</div>
<div style="text-align: left;padding-bottom: 10px">
    <div>{{ trans('person.customer') }} :{{ $customer->name }}</div>
    <div>{{ trans('person.phone') }} :{{ $customer->phone }}</div>
    <div>{{ trans('person.address') }} :{{ $customer->address }}</div>
    <div>{{ trans('order.employee') }}: {{ Auth::user()->name }} </div>
</div>
<table  style="border-collapse: collapse; border: 0px;margin: 0 auto;width: 100%">
    <tr style="border: 1px solid;">
        <th class="border" width="15%">{{ trans('usually.order') }}</th>
        <th class="border" width="30%">{{ trans('product.name') }}</th>
        <th class="border" width="15%">{{ trans('product.quantity') }}</th>
        <th class="border" width="20%">{{ trans('product.price') }}</th>
        <th class="border" width="20%">{{ trans('order.total_money') }}</th>
    </tr>
    @php($stt = 1)
    @foreach($products as $product)
    <tr>
        <td class="border">{{ $stt++ }}</td>
        <td class="border">{{ $product['item']['name'] }}</td>
        <td class="border" >{{ $product['qty'] }}</td>
        <td class="border">{{ formatPrice($product['item']['price']) }}</td>
        <td class="border">{{ formatPrice($product['price']) }}</td>
    </tr>
    @endforeach
</table>
<div style="text-align:right;padding-top: 20px">{{ trans('order.total') }} : {{formatPrice($totalPrice)}}</div>
<div style="font-style:italic;text-align:right;padding-top: 20px">{{ trans('order.thank_u') }}</div>
</body>
</html>
