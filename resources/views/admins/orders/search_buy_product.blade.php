@if($products->count() > 0)
    <ul style="padding-left:10px ">
        @foreach($products as $product)
            <li class="product" data-id="{{ $product->id }}" data-img="{{ $product-> image}}"
                data-price="{{ $product->price }}"
                data-sale="{{ $product->sale }}" data-name="{{ $product->name }}"
                data-quantity="{{ $product->quantity }}">
                <div><img width="58px" height="58px" src="/images/products/{{ $product->image }}"
                          alt=""></div>
                <div class="infor">
                    <p><strong>{{ $product->name }}</strong></p>
                    @if($product->sale != null)
                        <p>{{ trans('product.price') }} :<strike>{{ formatPrice($product->price) }}</strike>
                            | {{ formatPriceSale($product->price,$product->sale) }} <span class="label label-success">-{{$product->sale}} %</span>
                        </p>
                    @else
                        <p>{{ trans('product.price') }} :{{ formatPrice($product->price) }}</p>
                    @endif
                    <p>{{ trans('product.quantity') }} : {{ $product->quantity }}</p>
                </div>
            </li>
        @endforeach
    </ul>
@else
    <div style="padding: 8px">{{ trans('product.not_available') }}</div>
@endif

