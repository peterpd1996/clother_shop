@if($products !== null)
    @foreach($products as $product)
        <tr class="even pointer">
            <td>{{ $product['item']['id'] }}</td>
            <td><img width="58px" height="58px" src="/images/products/{{ $product['item']['image'] }}"
                     alt=""></td>
            <td>{{ $product['item']['name'] }}</td>
            <td><input type="number" style="width: 60px;" min="1" class="form-control quantity"
                       value="{{ $product['qty'] }}" data-id="{{ $product['item']['id'] }}"></td>
            <td>{{ formatPrice($product['item']['price']) }}</td>
            <td>{{ formatPrice($product['price']) }}</td>
            <td>
                <div class="btn btn-danger btn-xs btn-remove-item-cart" data-id="{{ $product['item']['id'] }}"><i
                        class=" fa fa-trash-o"></i> {{ trans('usually.delete') }} </div>
            </td>
        </tr>
    @endforeach
    <input type="hidden" id="totalPrice" value="{{ formatPrice($totalPrice) }}">
@endif

