@extends('admins.layouts.app')
@section('content')
    <div class="x_panel">
        <div class="row justify-content-center x_title">
            <div class="alert alert-success alert-dismissible" role="alert">
                <span style="color: white;text-transform: uppercase"><i class="fa fa-info" aria-hidden="true"></i> {{ trans('order.invoice') }} </span>
            </div>
        </div>
        <div class="x_content" style="color:black">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-6">
                    <div style="font-size: 39px;font-style: italic;padding-left: 90px">D'shop</div>
                    <div style="width: 380px;font-size: 20px;font-style: italic;">{{ trans('order.best_place_provide') }}</div>

                </div>
                <div class="col-md-3">
                    <div><i class="fa fa-phone" aria-hidden="true"></i> : 033052100</div>
                    <div><i class="fa fa-map-marker" aria-hidden="true"></i> : 88, lai xá, kim chung</div>
                    <div><i class="fa fa-facebook-square" aria-hidden="true"></i>: facebook/phone</div>
                </div>
            </div>
            <div class="row" style="padding-top: 20px">
                <div  style="text-align: center">
                    <div><h1>{{ trans('order.invoice') }}</h1></div>
                    <div>{{ trans('order.sale_day') }} : {{ date_format($order->created_at,"d-m-Y") }}</div>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8" id="order-table">

                    <table class="table table-striped jambo_table bulk_action" style="margin-top: 10px">
                        <thead>
                        <tr class="headings">
                            <th class="column-title">{{ trans('usually.order') }}</th>
                            <th class="column-title">{{ trans('product.name') }}</th>
                            <th class="column-title">{{ trans('product.quantity') }}</th>
                            <th class="column-title">{{ trans('product.price') }}</th>
                            <th class="column-title">{{ trans('product.sale') }}</th>
                            <th class="column-title">{{ trans('order.real_price') }} </th>
                            <th class="column-title">{{ trans('order.total_money') }}</th>
                        </tr>
                        </thead>
                        <tbody id="tbody">
                        @php($stt = 1)
                        @foreach($order->products as $product)
                            <tr class="even pointer">
                                <td>{{ $stt++ }}</td>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->pivot->quantity }}</td>
                                <td>{{formatPrice( $product->price) }}</td>
                                <td><span class="label label-success">{{ $product->pivot->sale ?? 0 }} %</span></td>
                                <td>{{ formatPrice($product->pivot->price ) }}</td>
                                <td>{{ formatPrice($product->pivot->quantity * $product->pivot->price ) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>


@endsection



