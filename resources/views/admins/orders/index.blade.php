@extends('admins.layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <nav aria-label="breadcrumb" style="font-size: 17px">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item active" aria-current="page">{{ trans('order.invoice') }}</li>
                            <li class="breadcrumb-item " aria-current="page">{{ trans('usually.list') }}</li>
                        </ol>
                    </nav>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="input-group">
                                <input style="border-radius: 18px" type="text" class="form-control search"
                                       placeholder="{{ trans('product.quick_search') }} ..."
                                       size="40" id="quick-search" data-url="{{route('admin.products.quick_search')}}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="x_content">
                    <div class="row" id="table-product">
                        <table class="table table-striped jambo_table bulk_action">
                            <thead>
                            <tr class="headings">
                                <th class="column-title">{{ trans('usually.id') }}</th>
                                <th class="column-title">{{ trans('order.employee') }}</th>
                                <th class="column-title">{{ trans('person.name') }}</th>
                                <th class="column-title">{{ trans('person.address') }}</th>
                                <th class="column-title">{{ trans('person.phone') }}</th>
                                <th class="column-title">{{ trans('order.total') }}</th>
                                <th class="column-title">{{ trans('order.sale_day') }}</th>
                                <th class="column-title">{{ trans('usually.action') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($orders as $order)
                                <tr class="even pointer ">
                                    <td>{{ $order->id }}</td>
                                    <td>{{ $order->user->name }}</td>
                                    <td>{{ $order->customer->name }}</td>
                                    <td>{{ $order->customer->address }}</td>
                                    <td>{{ $order->customer->phone }}</td>
                                    <td>{{ formatPrice($order->total_price) }}</td>
                                    <td>{{ date_format($order->created_at,"d-m-Y") }}</td>

                                    <td>
                                        <a href="{{ route('admin.orders.order_detail',['id'=>$order->id]) }}"
                                           class="btn btn-success btn-xs btn-edit-product"><i class="fa fa-eye"
                                                                                              aria-hidden="true"></i>{{ trans('usually.look') }}
                                        </a>
                                        <form style="display: inline"
                                              action="{{ route('admin.orders.destroy',['id'=>$order->id]) }}"
                                              method="post">
                                            @method('delete')
                                            @csrf
                                            <button type="submit" class="btn btn-danger btn-xs btn-delete-product"><i
                                                    class=" fa fa-trash-o"></i> {{ trans('usually.delete') }} </button>

                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div>{!! $orders->links() !!}  </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

