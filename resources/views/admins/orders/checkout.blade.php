@extends('admins.layouts.app')
@section('content')
    <div class="x_panel">
        <div class="row justify-content-center x_title">
            <div class="alert alert-success alert-dismissible" role="alert">
                <span style="color: white;text-transform: uppercase"><i class="fa fa-cart-arrow-down" style="font-size: 22px"></i>  {{ trans('order.checkout') }}  </span>
            </div>
        </div>
        <div class="x_content" style="color:black">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-6">
                    <div style="font-size: 39px;font-style: italic;padding-left: 90px">D'shop</div>
                    <div style="width: 380px;font-size: 20px;font-style: italic;">{{ trans('order.best_place_provide') }}</div>

                </div>
                <div class="col-md-3">
                    <div><i class="fa fa-phone" aria-hidden="true"></i> : 033052100</div>
                    <div><i class="fa fa-map-marker" aria-hidden="true"></i> : Số nhà 88, lai xá, kim chung</div>
                    <div><i class="fa fa-facebook-square" aria-hidden="true"></i>: facebook/phone</div>
                </div>
            </div>
            <div class="row" style="padding-top: 20px">
                <div  style="text-align: center">
                    <div style="text-transform: uppercase"><h1>{{ trans('order.invoice') }}</h1></div>
                    <div>{{ trans('order.sale_day') }} : {{ date('d-m-Y') }}</div>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-8" id="order-table">
                    <div>{{ trans('person.name') }}: {{ $customer->name }}</div>
                    <div>{{ trans('person.phone') }}: {{ $customer->phone }}</div>
                    <div>{{ trans('person.address') }}: {{ $customer->address }}</div>
                    <div>{{ trans('order.employee') }}: {{ Auth::user()->name }}</div>
                    <table class="table table-striped jambo_table bulk_action" style="margin-top: 10px">
                        <thead>
                        <tr class="headings">

                            <th class="column-title">{{ trans('usually.order') }}</th>
                            <th class="column-title">{{ trans('product.name') }}</th>
                            <th class="column-title">{{ trans('product.quantity') }}</th>
                            <th class="column-title">{{ trans('product.price') }}</th>
                            <th class="column-title">{{ trans('order.total_money') }}</th>
                        </tr>
                        </thead>
                        <tbody id="tbody">
                        @php($stt = 1)
                        @foreach($products as $product)
                            <tr class="even pointer">
                                <td>{{ $stt++ }}</td>
                                <td>{{ $product['item']['name'] }}</td>
                                <td>{{ $product['qty'] }}</td>
                                <td>{{ formatPrice($product['item']['price']) }}</td>
                                <td>{{ formatPrice($product['price']) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <div class="toal" style="text-align: right">{{ trans('order.total') }}: {{ formatPrice($totalPrice) }}</div>
                    <a href="{{ route('admin.orders.pdf',['customer_id' => $customer->id]) }}" class="btn btn-success" style="text-align: right"><i class="fa fa-print" aria-hidden="true"></i>  {{ trans('order.print_pdf') }}</a>
                </div>
            </div>
        </div>
    </div>


@endsection



